<?php

namespace semyon_san\yii2\models;

use Throwable;
use yii\base\DynamicModel;
use yii\db\BaseActiveRecord;

abstract class ModelWrapperForm extends DynamicModel
{
    /** @var BaseActiveRecord */
    protected $model;

    protected abstract function innerSave(): bool;

    public function attributeLabels(): array
    {
        return array_merge($this->model->attributeLabels(), parent::attributeLabels());
    }

    public function __construct(BaseActiveRecord $model)
    {
        $this->model = $model;
        parent::__construct($this->model->attributes, []);
    }

    /**
     * @throws Throwable
     */
    public function save(): bool
    {
        if (!$this->validate()) {
            return false;
        }

        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $this->model->load($this->getAttributes($this->safeAttributes()), '');
            if (!$this->innerSave()) {
                $transaction->rollBack();
                return false;
            }
            $transaction->commit();

        } catch (Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }

        return true;
    }

    public final function getModel(): BaseActiveRecord
    {
        return $this->model;
    }
}