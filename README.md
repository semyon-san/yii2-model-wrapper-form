# yii2-model-wrapper-form

This extension provides a package that implements a form model that saves ActiveRecord model that it wraps.

```bash
$ composer require semyon-san/yii2-model-wrapper-form
```

or add

```
"semyon-san/yii2-model-wrapper-form": "^<latest version>"
```

to the `require` section of your `composer.json` file.

## Configuration
To use the model, just extend in your own form models.

## Credits
- [Semyon](https://gitlab.com/semyon-san)

## License

The MIT License (MIT). Please see [LICENSE](https://github.com/jc-it/yii2-model-magic/blob/master/LICENSE) for more information.
